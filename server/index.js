const express = require('express');
const cors = require('cors');
const body_parser = require('body-parser');
const app = express();
const https = require("https");
const OpenAPI = require('@tinkoff/invest-openapi-js-sdk');

let tksapi = null;

app.use(cors());
app.use(body_parser());

app.get('/api/data', (req, res) => {
	var query = [];
	for (var prop in req.query) {
		query.push(prop + "=" + req.query[prop]);
	}
	const url = `https://ib17.hip-hop.ru/api/round/table?${query.join("&")}`;
	https.get(url, result => {
		result.setEncoding("utf8");
		let body = "";

		result.on("data", (data) => {
			body += data;
		});
		result.on("end", () => {
			body = JSON.parse(body);
			return res.json(body);
		});
	});
});

app.get('/api/login', (req, res) => {
	const apiURL = 'https://api-invest.tinkoff.ru/openapi';
	const sandboxApiURL = 'https://api-invest.tinkoff.ru/openapi/sandbox/';
	const socketURL = 'wss://api-invest.tinkoff.ru/openapi/md/v1/md-openapi/ws';

	tksapi = new OpenAPI({ apiURL: ('sandbox' in req.query ? sandboxApiURL : apiURL), secretToken: req.query.key, socketURL });

	let instruments = [];
	tksapi.stocks()
	.then(stocks => {
		instruments = stocks.instruments;
		return tksapi.bonds();
	})
	.then(bonds => {
		instruments = instruments.concat(bonds.instruments);
		return tksapi.etfs();
	})
	.then(etfs => {
		res.send(instruments.concat(etfs.instruments));
	})
	.catch(e => res.send('ERROR'));
});

app.get('/api/candles', (req, res) => {
	let to = new Date(parseInt(req.query.finishDate)),
		from = new Date(parseInt(req.query.finishDate)),
		interval = '';

	if (req.query.period === 'day') {
		interval = '10min';
		from.setDate(from.getDate() - 1);
	}
	if (req.query.period === 'week') {
		interval = 'hour';
		from.setDate(from.getDate() - 7);
	}
	if (req.query.period === 'month' || req.query.period === '2months') {
		interval = 'day';
		from.setDate(from.getDate() - 62);
	}
	if (req.query.period === 'year') {
		interval = 'week';
		from.setDate(from.getDate() - 366);
	}

	tksapi.candlesGet({
		from: from.toISOString(),
		to: to.toISOString(),
		interval: interval,
		figi: req.query.figi
	})
	.then(data => res.send(data))
	.catch(e => {
		res.send(e);
		console.log('ОШИБКА ПРИ ПОЛУЧЕНИИ СВЕЧЕЙ:');
		console.log(e);
		console.log(req.query);
	});
});

app.listen(4000, () => {
	console.log('server is listening on http://localhost:4000/');
});