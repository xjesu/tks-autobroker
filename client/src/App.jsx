import React from 'react';
import {useState} from 'react';
import './App.css';

function Login(props) {
	let key = '',
		stocks = null;

	function Login(key, sandbox) {
		fetch('/api/login?key=' + key + (sandbox ? '&sandbox' : ''))
		.then(responce => responce.text())
		.then(responce => {
			if (responce === 'ERROR') {
				alert('login failed');
			} else {
				stocks = JSON.parse(responce).filter(ins => ins.currency === 'RUB');
				props.onSuccess(stocks);
			}
		});
	}


	function HandleEnterKey(event, sandbox) {
		if (event.key === 'Enter') {
			Login(key, sandbox);
		}
	}

	return (
		<div>
			<input type="password" placeholder="tks api sandbox key" onChange={e => key = e.target.value} onKeyPress={e => HandleEnterKey(e, true)} />
			 or 
			<input type="password" placeholder="tks api key" onChange={e => key = e.target.value} onKeyPress={e => HandleEnterKey(e)} />
		</div>
	);
}

class Instrument extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			name: props.data.name,
			ticker: props.data.ticker,
			figi: props.data.figi
		};
		this.graph = <canvas ref={(c) => {
			this.canvas = c;
			this.ctx = c.getContext('2d')
		}} width={0} height={22}/>;
	}
	render() {
		return <div className="instrument">
			<div className="canvas-wrap">{this.graph}</div>
			<div className="ticker">{this.state.ticker}</div>
			<a href={'#' + this.state.ticker} className="name">{this.state.name}</a>
		</div>;
	}
	updateCandles(data, period, finishDate) {
		let HEIGHT = this.canvas.height,
			min = 99999999999,
			max = 0;

		data.candles.map(candle => {
			min = Math.min(candle.l, min);
			max = Math.max(candle.h, max);
		});

		window.candles[this.state.ticker] = data.candles;

		this.canvas.width = data.candles.length;
		this.ctx.fillStyle = 'black';
		for (let i = 0; i < data.candles.length; i++) {
			let low = (data.candles[i].l - min) / (max - min) * this.canvas.height,
				high = (data.candles[i].h - min) / (max - min) * this.canvas.height;
			this.ctx.fillRect(i, HEIGHT - low, 1, (high - low));
		}
	}
}

class Instruments extends React.Component {
	constructor(props) {
		super(props);
		this.state = {instruments: props.list};
		this.period = 'week';
		this.updateInterval = 1;
		window.candles = [];
	}
	componentDidMount() {
		this.updateCandlesLoop(0, this.period, new Date());
	}
	render() {
		this.refs = [];
		let items = this.state.instruments.map(instrument => {
			let ref = React.createRef();
			this.refs.push(ref);
			return <Instrument ref={ref} key={instrument.figi} data={instrument} />
		});
		return <div className="instruments">
			<div className="list">
				<label><input type="radio" name="period" onChange={() => this.period = "day"} />day</label>
				<label><input type="radio" name="period" onChange={() => this.period = "week"} checked={true} />week</label>
				<label><input type="radio" name="period" onChange={() => this.period = "month"} />month</label>
				<label><input type="radio" name="period" onChange={() => this.period = "year"} />year</label>
			</div>
			<div className="list">
				{items}
			</div>
		</div>
	}
	updateCandlesLoop(nthInstrument, period, finishDate) {
		if (!this.refs[nthInstrument]) {
			nthInstrument = 0;
		}

		fetch('api/candles?figi=' + this.refs[nthInstrument].current.state.figi + '&period=' + period + '&finishDate=' + finishDate.getTime())
		.then(responce => responce.json())
		.then(json => {
			this.refs[nthInstrument].current.updateCandles(json, period, finishDate);
			setTimeout((n, period, finishDate) => this.updateCandlesLoop(n, period, finishDate),
				this.updateInterval,
				nthInstrument + 1,
				this.period,
				new Date());
		});
	}
}

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			screen: 'login',
			instruments: null
		};
	}
	render() {
		if (this.state.screen === 'login') {
			return <Login onSuccess={instruments => this.setState({screen: 'instruments', instruments: instruments})} />;
		}
		if (this.state.screen === 'instruments') {
			this.list = <Instruments list={this.state.instruments} />;
			return this.list;
		}
		return <div>Error</div>;
	}
}

export default App;
